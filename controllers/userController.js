const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
};

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		if(result == null){

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			} else {

				return false
				
			};

		};
	});
};

module.exports.createOrder = async (data) => {

	if(!data.user.isAdmin){
		let notFound = false;
		let productId = null;
		let calculated = 0;

		for(let i=0; i<data.products.length; i++){

			await Product.findById(data.products[i].productId).then((product, err) => {
				if(err){
					notFound = true;
					productId = data.products[i].productId;
				} else {
					calculated += product.price*data.products[i].quantity;
				}
			});

			if(notFound){
				break;
			}
		}

		if(notFound){
			let message = Promise.resolve(`Product with ProductID-${productId} not found`);
			return message.then((value) => {
				return value
			});
		}

		let newOrder = new Order({
			userId: data.user.id,
			products: data.products,
			totalAmount: calculated
		});

		return newOrder.save().then((order, err) => {

			if(err){
				return false;
			} else {
				return true;
			}
		});
	}

	let message = Promise.resolve("User must not be an Admin to access this!");
	return message.then((value) => {
		return value
	});
};

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	})
};

module.exports.makeAdmin = (isAdmin, id) => {

	let updatedUser = {
		isAdmin: true
	}

	if(isAdmin){
		return User.findByIdAndUpdate(id, updatedUser).then((user, err) => {

			if(err){
				return "User with given id not found"
			} else {
				return `Successfully made the user with id-${id} as admin`
			}
	    })
	}

	let message = Promise.resolve("Only admin can access this feature!!");
	return message.then((value) => {
		return value
	});
}

module.exports.getUserOrders = (id) => {

	return Order.find({userId: id}).then((result, err) => {

		if(err){
			return "Unable to get your orders!!"
		} else {
			return result;
		}
	})
}