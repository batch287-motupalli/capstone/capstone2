const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

router.post("/addProduct", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

router.get("/retrieveAll", auth.verify ,(req, res) => {

	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/retrieveActive", auth.verify, (req, res) => {

	productController.activeProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/:productId", auth.verify, (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId", auth.verify, (req, res) => {

	let isAdmin = auth.decode(req.headers.authorization).isAdmin;

	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

router.patch("/:productId/archive", auth.verify, (req, res) => {	

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productId
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;