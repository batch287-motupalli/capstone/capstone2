const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/checkout", auth.verify,(req, res) => {
		
	let data = {
		user: auth.decode(req.headers.authorization),
		products: req.body
	};

	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({ userId: userData.id }).then(resultFromController => res.send(resultFromController));
});

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	userController.makeAdmin(isAdmin, req.params.userId).then(resultFromController => res.send(resultFromController));
});

router.get("/retrieveMyOrders", auth.verify, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id;

	userController.getUserOrders(userId).then(resultFromController => res.send(resultFromController));
});



module.exports = router;